<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>fr.sgued.ten_forward</id>
  <launchable type="desktop-id">fr.sgued.ten_forward.desktop</launchable>
  <url type = "homepage">https://ten-forward.sgued.fr</url>
  <url type = "bugtracker">https://gitlab.com/sgued/ten-forward/-/issues</url>
  <url type = "contribute">https://gitlab.com/sgued/ten-forward</url>
  <name>Ten Forward</name>
  <summary>Control a NAT-PMP gateway</summary>
  <description>
    <p>Ten Forward allows you to request port forwarding to your gateway.</p>

    <p>It allows you to create multiple mapping configurations, which can individually be requested. This tool will allow you to run a local game server (minecraft LAN, Mindustry, Stardew valley., etc..) and make it accessible online. It is tested with the NAT-PMP enabled ProtonVPN, and should be compatible with any other NAT-PMP capable gateway.</p>
   </description>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>AGPL-3.0+</project_license>
  <content_rating type="oars-1.1" />
  <releases>
    <release version="0.4.3" date="2024-12-18">
      <description>
        <ul>
          <li>Update dependencies</li>
          <li>No user-facing changes</li>
        </ul>
      </description>
    </release>
    <release version="0.4.2" date="2024-10-23">
      <description>
        <ul>
          <li>Fix minor bugs</li>
        </ul>
      </description>
    </release>
    <release version="0.4.1" date="2024-09-25">
      <description>
        <ul>
          <li>Improve Dialogs and spinners</li>
        </ul>
      </description>
    </release>
    <release version="0.4.0" date="2024-08-03">
      <description>
        <ul>
          <li>Minor UI improvements</li>
        </ul>
      </description>
    </release>
    <release version="0.3.1" date="2024-07-15">
      <description>
        <ul>
          <li>Minor fixes</li>
        </ul>
      </description>
    </release>
    <release version="0.3.0" date="2024-04-20">
      <description>
        <ul>
          <li>Added Branding colors</li>
          <li>Improved Translations</li>
        </ul>
      </description>
    </release>
    <release version="0.2.2" date="2024-01-20">
      <description>
        <ul>
          <li>Improved screenshots on website and flathub</li>
          <li>Improved Flathub software page</li>
          <li>Clear the "new mapping" page when a new mapping is created</li>
          <li>Make the time between requests more dynamic. This should allow for less frequent requests and improve support for gateways with very short mapping lifespans</li>
          <li>Reduced the opening size of the Window to have less empty space</li>
        </ul>
      </description>
    </release>
    <release version="0.2.1" date="2023-11-07">
      <description>
        <ul>
          <li>Fixed translations for the "About" button</li>
        </ul>
      </description>
    </release>
    <release version="0.2.0" date="2023-11-07">
      <description>
        <ul>
          <li>Added french translation</li>
          <li>Improved UI</li>
          <li>Set default type of new mappings to be TCP + UDP</li>
        </ul>
      </description>
    </release>
    <release version="0.1.0" date="2023-11-01">
      <description>
        <p>First release</p>
      </description>
    </release>
  </releases>
  <branding>
    <color type="primary" scheme_preference="light">#0bc1fc</color>
    <color type="primary" scheme_preference="dark">#118ab2</color>
  </branding>
  <keywords>
    <keyword>Internet</keyword>
    <keyword>Port</keyword>
    <keyword>Forwarding</keyword>
    <keyword>Gaming</keyword>
  </keywords>
  <screenshots>
    <screenshot type = "default" environment="gnome">
      <image type = "source">https://gitlab.com/sgued/ten-forward/-/raw/main/assets/screenshot-gateway.en.png</image>
    </screenshot>
    <screenshot environment="gnome">
      <image type = "source">https://gitlab.com/sgued/ten-forward/-/raw/main/assets/screenshot-new-mapping.en.png</image>
    </screenshot>
    <screenshot environment="gnome">
      <image type = "source">https://gitlab.com/sgued/ten-forward/-/raw/main/assets/screenshot-mindustry.en.png</image>
    </screenshot>
    <screenshot environment="gnome:dark">
      <image type = "source">https://gitlab.com/sgued/ten-forward/-/raw/main/assets/screenshot-gateway-dark.en.png</image>
    </screenshot>
    <screenshot environment="gnome:dark">
      <image type = "source">https://gitlab.com/sgued/ten-forward/-/raw/main/assets/screenshot-new-mapping-dark.en.png</image>
    </screenshot>
    <screenshot environment="gnome:dark">
      <image type = "source">https://gitlab.com/sgued/ten-forward/-/raw/main/assets/screenshot-mindustry-dark.en.png</image>
    </screenshot>
  </screenshots>
  <developer id="fr.sgued">
    <name>Sosthène Guédon</name>
  </developer>
  <update_contact>sosthene@guedon.gdn</update_contact>
</component>
