use std::net::Ipv4Addr;
use std::time::Duration;

use futures_concurrency::future::Race;
use i18n_embed_fl::fl;
use natpmp_ng::{GatewayResponse, NatpmpAsync, Protocol, Response};
use relm4::Sender;
use tokio::net::UdpSocket;
use tokio::sync::mpsc::UnboundedReceiver;
use tokio::time::{sleep_until, timeout, Instant};
use tracing::{debug, error, info, trace, warn};
use uuid::Uuid;

use super::LOCALE;
use crate::state::Type;

const DEFAULT_REFRESH: Duration = Duration::from_secs(3600);

#[derive(Debug)]
pub enum Message {
    UseGateway(Ipv4Addr),
    AddMapping {
        uuid: Uuid,
        private_port: u16,
        public_port: u16,
        ty: Type,
    },
    RemoveMapping(Uuid),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Ord, PartialOrd)]
enum State {
    Running,
    ToDelete,
    ToStart,
}

impl State {
    fn is_to_delete(self) -> bool {
        matches!(self, Self::ToDelete)
    }
    fn is_to_start(self) -> bool {
        matches!(self, Self::ToStart)
    }
}

#[derive(Debug, Clone)]
pub struct Mapping {
    uuid: Uuid,
    state: State,
    private_port: u16,
    public_port: u16,
    ty: Type,
    last_was_error: bool,
}

const ASK_FOR: u32 = 3600; // 1h
const TIMEOUT: Duration = Duration::from_millis(10000);

#[tracing::instrument]
pub(crate) async fn run_backend(
    mut receiver: UnboundedReceiver<Message>,
    mut sender: Sender<crate::Message>,
    mut gateway: Option<Ipv4Addr>,
) {
    let mut previous_start = Instant::now();

    let mut natpmp: Option<NatpmpAsync<UdpSocket>> = None;
    let mut running_mappings: Vec<Mapping> = Vec::new();
    let mut should_update_ip = true;

    // The smallest time that was allocated during the previous refresh;
    let mut run_every = Duration::from_secs(ASK_FOR as u64);

    loop {
        let sleep = async move {
            if !should_update_ip {
                sleep_until(previous_start + run_every).await;
            }
            false
        };

        previous_start = Instant::now();

        trace!(
            "Backend loop, natpmp.is_some(): {}, preparing to sleep for {run_every:?}",
            natpmp.is_some()
        );
        let recv = async {
            let Some(msg) = receiver.recv().await else {
                return true;
            };
            debug!("Backend received: {msg:?}");

            match msg {
                Message::UseGateway(new_gateway) => {
                    gateway = Some(new_gateway);
                    should_update_ip = true;
                }
                Message::AddMapping {
                    uuid,
                    private_port,
                    public_port,
                    ty,
                } => {
                    assert!(natpmp.is_some());
                    running_mappings.push(Mapping {
                        uuid,
                        state: State::ToStart,
                        private_port,
                        public_port,
                        ty,
                        last_was_error: false,
                    });
                }
                Message::RemoveMapping(uuid) => running_mappings.iter_mut().for_each(|m| {
                    if m.uuid == uuid {
                        m.state = State::ToDelete;
                    }
                }),
            }

            false
        };

        if (sleep, recv).race().await {
            info!("Leaving loop");
            break;
        }

        if let Some(gateway) = gateway.take() {
            info!("Updating natpmp");
            natpmp = natpmp_ng::new_tokio_natpmp_with(gateway)
                .await
                .map_err(|err| {
                    warn!("Failed to start NatPMP: {err}");
                    sender
                        .send(crate::Message::GatewayError(fl!(
                            LOCALE,
                            "failed-to-start",
                            err = err.to_string(),
                        )))
                        .ok();
                })
                .ok();
            previous_start = Instant::now();
        } else if natpmp.is_none() {
            info!("Updating natpmp with default");
            natpmp = natpmp_ng::new_tokio_natpmp()
                .await
                .map_err(|err| {
                    warn!("Failed to start NatPMP: {err}");
                    sender
                        .send(crate::Message::GatewayError(fl!(
                            LOCALE,
                            "failed-to-start",
                            err = err.to_string(),
                        )))
                        .ok();
                })
                .ok();
        }

        if let Some(natpmp) = natpmp.as_mut() {
            if should_update_ip {
                info!("Getting public address");
                let res = async {
                    natpmp.send_public_address_request().await.map_err(|err| {
                        error!("Failed to send request: {err}");
                    })?;
                    timeout(TIMEOUT, async {
                        natpmp.read_response_or_retry().await.map_err(|err| {
                            error!("Failed to read response: {err}");
                        })
                    })
                    .await
                    .map_err(|_err| {
                        warn!("Timed out while attempting to read response");
                    })?
                }
                .await;
                info!("got response");
                should_update_ip = false;

                match res {
                    Ok(natpmp_ng::Response::Gateway(r)) => {
                        info!("Got public address");
                        sender
                            .send(crate::GatewayMessage::PublicIp(Some(*r.public_address())).into())
                            .map_err(|_| error!("Frontend closed"))
                            .ok();
                    }
                    Ok(r) => {
                        warn!("Got response for other data type: {r:?}");
                        sender
                            .send(crate::GatewayMessage::PublicIp(None).into())
                            .map_err(|_| error!("Frontend closed"))
                            .ok();
                    }
                    Err(_) => {
                        info!("Failed to communicate with gateway");
                        sender
                            .send(crate::GatewayMessage::PublicIp(None).into())
                            .map_err(|_| error!("Frontend closed"))
                            .ok();
                    }
                }
            }
        }

        let Some(natpmp) = natpmp.as_mut() else {
            warn!("Running mappings without a natpmp instance running");
            continue;
        };

        let mut min_allocated_time = DEFAULT_REFRESH;
        for m in &mut running_mappings {
            let mut callback = |response: GatewayResponse| {
                sender
                    .send(crate::Message::Gateway(crate::GatewayMessage::PublicIp(
                        Some(*response.public_address()),
                    )))
                    .map_err(|_| error!("Frontend closed"))
                    .ok();
            };
            if m.state.is_to_delete() {
                if m.ty.tcp() {
                    run_delete_request(m, natpmp, Protocol::TCP, &mut callback).await;
                }
                if m.ty.udp() {
                    run_delete_request(m, natpmp, Protocol::UDP, &mut callback).await;
                }
                sender
                    .send(crate::Message::Mapping(
                        m.uuid,
                        crate::mapping::Message::BackendStopped,
                    ))
                    .ok();
            } else {
                let allocated_time = run_port_request(m, natpmp, &mut sender).await;
                min_allocated_time = allocated_time.min(min_allocated_time);
            }
        }
        running_mappings.retain(|m| !m.state.is_to_delete());
        run_every = min_allocated_time;
    }

    info!("Closing backend");
}

async fn get_response_with_ignores(
    natpmp: &mut NatpmpAsync<UdpSocket>,
    protocol: Protocol,
    private_port: u16,
    public_port: u16,
    lifetime: u32,
    on_gateway: &mut impl FnMut(GatewayResponse),
) -> Result<(u16, Duration), natpmp_ng::Error> {
    let res = timeout(TIMEOUT, async {
        loop {
            natpmp
                .send_port_mapping_request(protocol, private_port, public_port, lifetime)
                .await?;
            tracing::info!("Sent port mapping request");
            match (natpmp.read_response_or_retry().await?, protocol) {
                (Response::Gateway(g), _) => on_gateway(g),
                (Response::UDP(udp), Protocol::UDP) if udp.private_port() == private_port => {
                    info!("Got udp lifetime: {}s", udp.lifetime().as_secs());
                    return Ok((udp.public_port(), *udp.lifetime()));
                }
                (Response::TCP(tcp), Protocol::TCP) if tcp.private_port() == private_port => {
                    info!("Got tcp lifetime: {}s", tcp.lifetime().as_secs());
                    return Ok((tcp.public_port(), *tcp.lifetime()));
                }
                (res, proto) => {
                    warn!("Got unexpected response: {res:?} for expected proto: {proto:?}")
                }
            }
        }
    })
    .await;

    match res {
        Ok(Ok(r)) => Ok(r),
        Ok(Err(err)) => Err(err),
        Err(_) => Err(natpmp_ng::Error::NATPMP_ERR_SOCKETERROR),
    }
}

async fn run_port_request(
    mapping: &mut Mapping,
    natpmp: &mut NatpmpAsync<UdpSocket>,
    sender: &mut Sender<crate::Message>,
) -> Duration {
    tracing::info!("Runnig port request for mapping: {mapping:?}");
    let started = mapping.state.is_to_start();
    let mut got_duration = DEFAULT_REFRESH;
    let action = async {
        let mut on_gateway = |response: GatewayResponse| {
            sender
                .send(crate::Message::Gateway(crate::GatewayMessage::PublicIp(
                    Some(*response.public_address()),
                )))
                .ok();
        };

        if mapping.ty.tcp() {
            let (port, tcp_duration) = get_response_with_ignores(
                natpmp,
                Protocol::TCP,
                mapping.private_port,
                mapping.public_port,
                ASK_FOR,
                &mut on_gateway,
            )
            .await?;
            got_duration = tcp_duration;
            mapping.public_port = port;
        }

        if mapping.ty.udp() {
            let (port, udp_duration) = get_response_with_ignores(
                natpmp,
                Protocol::UDP,
                mapping.private_port,
                mapping.public_port,
                ASK_FOR,
                &mut on_gateway,
            )
            .await?;
            got_duration = udp_duration.min(got_duration);
            if mapping.ty.tcp() && port != mapping.public_port {
                warn!("UDP public port is different from TCP port");
            } else {
                mapping.public_port = port;
            }
        }

        if started {
            mapping.state = State::Running;
            sender
                .send(crate::Message::Mapping(
                    mapping.uuid,
                    crate::mapping::Message::Running(mapping.public_port),
                ))
                .ok();
        }

        if mapping.last_was_error {
            sender
                .send(crate::Message::Mapping(
                    mapping.uuid,
                    crate::mapping::Message::Running(mapping.public_port),
                ))
                .ok();
        }
        mapping.last_was_error = false;

        Ok(())
    };

    if let Err(err) = action.await {
        let _: natpmp_ng::Error = err;
        mapping.last_was_error = true;
        sender
            .send(crate::Message::Mapping(
                mapping.uuid,
                crate::mapping::Message::Error(fl!(
                    LOCALE,
                    "failed-to-run-request",
                    err = err.to_string()
                )),
            ))
            .ok();
    }
    got_duration
}

async fn run_delete_request(
    mapping: &Mapping,
    natpmp: &mut NatpmpAsync<UdpSocket>,
    protocol: Protocol,
    on_gateway: &mut impl FnMut(GatewayResponse),
) {
    timeout(
        TIMEOUT,
        get_response_with_ignores(
            natpmp,
            protocol,
            mapping.private_port,
            mapping.public_port,
            0,
            on_gateway,
        ),
    )
    .await
    .ok();
}
