use adw::prelude::*;
use i18n_embed_fl::fl;
use relm4::{prelude::*, ComponentParts, ComponentSender, SimpleComponent};
use relm4_components::simple_adw_combo_row::{SimpleComboRow, SimpleComboRowMsg};
use tracing::{debug, error, warn};
use uuid::Uuid;

use crate::{
    state::{self, Type},
    ToastMessage, LOCALE,
};

#[derive(Debug, Clone)]
pub enum Message {
    Save {
        name: String,
        public_port: u16,
        private_port: u16,
    },
}

#[derive(Debug)]
pub struct NewMapping {
    combo_row: Controller<SimpleComboRow<Type>>,
}

#[derive(Debug, Clone)]
pub enum OutMessage {
    NewMapping(state::Mapping),
    Toast(ToastMessage),
}

#[relm4::component(pub)]
impl SimpleComponent for NewMapping {
    type Init = ();
    type Input = Message;
    type Output = OutMessage;

    view! {
        gtk::Box {
            set_margin_all: 10,
            set_spacing: 10,
            set_vexpand: false,
            set_hexpand: true,
            set_orientation: gtk::Orientation::Vertical,

            gtk::Button {
                set_label: &fl!(LOCALE, "save"),
                set_hexpand: true,
                connect_clicked[sender, name_edit, public_port_edit, private_port_edit] => move |_| {
                    let name = name_edit.text().into();
                    let Ok(public_port) = public_port_edit.text().parse() else {
                        warn!(public_port = public_port_edit.text().as_str(), "Got non numerical public port");
                        sender.output(OutMessage::Toast(ToastMessage::new(fl!(LOCALE, "ports-must-be-numbers-between-0-and-65535")))).ok();
                        return;
                    };
                    let Ok(private_port) = private_port_edit.text().parse() else {
                        warn!(private_port = private_port_edit.text().as_str(), "Got non numerical private port");
                        sender.output(OutMessage::Toast(ToastMessage::new(fl!(LOCALE, "ports-must-be-numbers-between-0-and-65535")))).ok();
                        return;
                    };

                    // All was parsed, therefore we can clear the widgets
                    name_edit.set_text("");
                    public_port_edit.set_text("");
                    private_port_edit.set_text("");

                    sender.input({
                        Message::Save {
                            name,
                            public_port,
                            private_port,
                        }
                    });
                }
            },

            adw::PreferencesGroup {
                #[name = "name_edit"]
                adw::EntryRow {
                    set_title: &fl!(LOCALE, "name"),
                },
                #[local_ref]
                // #[name = "type_select"]
                comborow -> adw::ComboRow,

                #[name = "public_port_edit"]
                adw::EntryRow {
                    set_title: &fl!(LOCALE, "public-port"),
                    set_input_purpose: gtk::InputPurpose::Digits,
                },
                #[name = "private_port_edit"]
                adw::EntryRow {
                    set_title: &fl!(LOCALE, "private-port"),
                    set_input_purpose: gtk::InputPurpose::Digits,
                },
            }
        }
    }

    fn init(_state: (), root: Self::Root, sender: ComponentSender<Self>) -> ComponentParts<Self> {
        let model = Self {
            combo_row: SimpleComboRow::builder()
                .launch(SimpleComboRow {
                    // Default is both, therefore first
                    variants: vec![Type::Both, Type::Tcp, Type::Udp],
                    active_index: Some(0),
                })
                .detach(),
        };

        let comborow = model.combo_row.widget();
        comborow.set_title(&fl!(LOCALE, "type"));
        let widgets = view_output!();
        ComponentParts { model, widgets }
    }

    fn update(&mut self, msg: Self::Input, sender: ComponentSender<Self>) {
        debug!("Got message: {msg:?}");
        match msg {
            Message::Save {
                name,
                public_port,
                private_port,
            } => {
                let ty = *self.combo_row.model().get_active_elem().unwrap();
                let uuid = Uuid::new_v4();
                sender
                    .output_sender()
                    .send(OutMessage::NewMapping(state::Mapping {
                        name,
                        uuid,
                        ports: state::PortMapping {
                            uuid,
                            public_port,
                            private_port,
                            ty,
                        },
                    }))
                    .map_err(|err| error!("Failed to send save: {err:?}"))
                    .ok();

                // Clear also the comborow
                self.combo_row
                    .sender()
                    .send(SimpleComboRowMsg::SetActiveIdx(0))
                    .ok();
            }
        }
    }
}
