use std::{
    fmt::Display,
    fs::{self, read_to_string},
    io::{self, ErrorKind},
    net::Ipv4Addr,
    path::PathBuf,
};

use directories::ProjectDirs;
use i18n_embed_fl::fl;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use tracing::{error, info};
use uuid::Uuid;

use super::LOCALE;

#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum Type {
    Tcp,
    Udp,
    Both,
}

impl Type {
    pub fn tcp(self) -> bool {
        matches!(self, Type::Tcp | Type::Both)
    }
    pub fn udp(self) -> bool {
        matches!(self, Type::Udp | Type::Both)
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct State {
    #[serde(default)]
    pub gateway: Option<Ipv4Addr>,
    #[serde(default)]
    pub mappings: Vec<Mapping>,
}

static APP_PROJECT_DIRS: Lazy<Option<ProjectDirs>> =
    Lazy::new(|| ProjectDirs::from(crate::APP_ID, "Sgued", "Ten Forward"));

impl State {
    pub fn load() -> Option<Self> {
        let Some(dirs) = &*APP_PROJECT_DIRS else {
            error!("Failed to get directories for storage");
            return None;
        };

        let mut path = PathBuf::from(dirs.data_dir());
        fs::create_dir_all(&path).ok();
        path.push("state.json");
        info!("Loading state from {path:?}");

        match read_to_string(path) {
            Ok(data) => {
                let state = serde_json::from_str(&data)
                    .map_err(|err| error!("State is corrupted: {err}"))
                    .ok();
                info!("Loaded state: {state:?}");
                state
            }
            Err(err) if err.kind() == ErrorKind::NotFound => Some(Self::default()),
            Err(err) => {
                error!("Failed to read {err}");
                None
            }
        }
    }

    pub fn save(&self) -> io::Result<()> {
        let Some(dirs) = &*APP_PROJECT_DIRS else {
            error!("Failed to get directories for storage");
            return Err(io::Error::new(
                ErrorKind::NotFound,
                "Could not get directories for storage",
            ));
        };

        let mut path = PathBuf::from(dirs.data_dir());
        path.push("state.json");
        let serialized = serde_json::to_string_pretty(self).expect("Serialization failed");
        fs::write(path, serialized)
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Copy)]
pub struct PortMapping {
    pub uuid: Uuid,
    pub public_port: u16,
    pub private_port: u16,
    pub ty: Type,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Mapping {
    pub name: String,
    pub uuid: Uuid,
    pub ports: PortMapping,
}

impl Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Udp => f.write_str(&fl!(LOCALE, "udp")),
            Self::Tcp => f.write_str(&fl!(LOCALE, "tcp")),
            Self::Both => f.write_str(&fl!(LOCALE, "tcp-and-udp")),
        }
    }
}
