use std::cell::Cell;

use crate::ToastMessage;
use adw::prelude::*;

use relm4::{prelude::*, ComponentParts, ComponentSender, SimpleComponent};
use relm4_components::simple_adw_combo_row::SimpleComboRow;

use i18n_embed_fl::fl;
use uuid::Uuid;

use crate::state::{self, Type};
use crate::LOCALE;

#[derive(Debug, Clone)]
pub enum Message {
    Edit,
    SaveEdit {
        name: String,
        public_port: u16,
        private_port: u16,
    },
    CancelEdit,
    Delete,
    Copy,
    StartStop,
    BackendStopped,
    Running(u16),
    Error(String),
}

#[derive(Debug, Clone)]
pub enum OutMessage {
    ChangeName(String),
    Start,
    Stop,
    Copy(u16),
    Delete,
    Save,
    Toast(ToastMessage),
    UpdateState(State),
}

pub struct MappingInit {
    pub uuid: Uuid,
    pub name: String,
    pub ports: state::PortMapping,
    pub window: adw::Window,
}

#[derive(Debug, Default, Copy, Clone)]
pub enum State {
    #[default]
    Idle,
    Starting,
    Running(u16),
    Stopping,
    Error,
}

impl State {
    fn port_or(&self, default: u16) -> u16 {
        match self {
            Self::Running(p) => *p,
            _ => default,
        }
    }

    fn is_running(&self) -> bool {
        !matches!(self, Self::Idle)
    }

    fn button_value(&self) -> String {
        match self.is_running() {
            true => fl!(LOCALE, "stop"),
            false => fl!(LOCALE, "start"),
        }
    }

    fn button_class(&self) -> &'static [&'static str] {
        match self.is_running() {
            false => &["suggested-action"],
            true => &["destructive-action"],
        }
    }

    fn title(&self) -> String {
        match self {
            Self::Idle => fl!(LOCALE, "idle"),
            Self::Starting => fl!(LOCALE, "starting"),
            Self::Running(_) => fl!(LOCALE, "running"),
            Self::Stopping => fl!(LOCALE, "stopping"),
            Self::Error => fl!(LOCALE, "error"),
        }
    }

    fn css_classes(&self) -> &'static [&'static str] {
        match self {
            Self::Idle | Self::Starting | Self::Stopping => &[],
            Self::Running(_) => &["property"],
            Self::Error => &["error"],
        }
    }

    fn activatable(&self) -> bool {
        matches!(self, Self::Running(_))
    }

    fn tooltip(&self) -> Option<String> {
        if matches!(self, Self::Running(_)) {
            Some(fl!(LOCALE, "copy-to-clipboard"))
        } else {
            None
        }
    }
}

pub struct MappingModel {
    pub name: String,
    pub port: state::PortMapping,
    state: State,
    pub editing: bool,
    pub combo_row: Controller<SimpleComboRow<Type>>,
    spinner: adw::Spinner,
    spinner_present: Cell<bool>,
    icon: gtk::Image,
    icon_present: Cell<bool>,
    window: adw::Window,
}

impl MappingModel {
    fn subtitle(&self) -> String {
        if let State::Running(port) = self.state {
            fl!(LOCALE, "obtained-public-port", port = port)
        } else {
            String::new()
        }
    }

    fn add_spinner(&self) -> Option<&adw::Spinner> {
        if !self.spinner_present.get() && matches!(self.state, State::Starting | State::Stopping) {
            self.spinner_present.set(true);
            Some(&self.spinner)
        } else {
            None
        }
    }
    fn remove_spinner(&self) -> Option<&adw::Spinner> {
        if self.spinner_present.get() && !matches!(self.state, State::Starting | State::Stopping) {
            self.spinner_present.set(false);
            Some(&self.spinner)
        } else {
            None
        }
    }
    fn add_copy_icon(&self) -> Option<&gtk::Image> {
        if !self.icon_present.get() && matches!(self.state, State::Running(_)) {
            self.icon_present.set(true);
            Some(&self.icon)
        } else {
            None
        }
    }
    fn remove_copy_icon(&self) -> Option<&gtk::Image> {
        if self.icon_present.get() && !matches!(self.state, State::Running(_)) {
            self.icon_present.set(false);
            Some(&self.icon)
        } else {
            None
        }
    }
}

#[relm4::component(pub)]
impl SimpleComponent for MappingModel {
    type Init = MappingInit;
    type Input = Message;
    type Output = OutMessage;
    view! {
        adw::Bin {

            #[transition = "SlideLeftRight"]
            if !model.editing {
                gtk::Box {
                    set_margin_all: 10,
                    set_orientation: gtk::Orientation::Vertical,
                    set_spacing: 10,
                    gtk::Box{
                        set_orientation: gtk::Orientation::Horizontal,
                        add_css_class: "linked",
                        #[name = "edit"]
                        gtk::Button {
                            set_hexpand: true,
                            set_label: &fl!(LOCALE, "edit"),
                            connect_clicked[sender] => move |_| {
                                sender.input(Message::Edit);
                            }
                        },
                        #[name = "start_stop"]
                        gtk::Button {
                            set_hexpand: true,
                            #[watch]
                            set_label: &model.state.button_value(),
                            #[watch]
                            set_css_classes: model.state.button_class(),
                            connect_clicked[sender] => move |_| {
                                sender.input(Message::StartStop);
                            }

                        },
                    },
                    gtk::ListBox {
                        add_css_class: "boxed-list",
                        set_hexpand: true,

                        adw::ActionRow {
                            #[watch]
                            set_title: &model.state.title(),
                            #[watch]
                            set_subtitle: &model.subtitle(),
                            #[watch]
                            set_tooltip_text: model.state.tooltip().as_deref(),
                            #[watch]
                            remove?: model.remove_spinner(),
                            #[watch]
                            remove?: model.remove_copy_icon(),
                            #[watch]
                            add_suffix?: model.add_spinner(),
                            #[watch]
                            add_suffix?: model.add_copy_icon(),
                            #[watch]
                            set_activatable: model.state.activatable(),
                            connect_activated[sender] => move |_| {
                                sender.input(Message::Copy);
                            },
                            #[watch]
                            set_css_classes: model.state.css_classes(),
                        },

                        #[name = "port"]
                        adw::ActionRow {
                            #[watch]
                            set_title: &format!("{}", model.port.ty),
                            add_css_class: "property",
                            #[watch]
                            set_subtitle: &fl!(LOCALE, "public-port-forlocal-port", public =  model.port.public_port, private = model.port.private_port),
                        }
                    }
                }
            } else {
                gtk::Box {
                    set_margin_all: 10,
                    set_orientation: gtk::Orientation::Vertical,
                    set_spacing: 10,
                    gtk::Box {
                        set_orientation: gtk::Orientation::Horizontal,
                        #[name = "delete"]
                        gtk::Button {
                            set_label: &fl!(LOCALE, "delete"),
                            add_css_class: "flat",
                            connect_clicked[sender] => move |_| {
                                sender.input(Message::Delete);
                            }
                        },
                        gtk::Box {
                            set_hexpand: true,
                        },
                        gtk::Box {
                            add_css_class: "linked",
                            #[name = "cancel"]
                            gtk::Button {
                                set_label: &fl!(LOCALE, "cancel"),
                                connect_clicked[sender] => move |_| {
                                    sender.input(Message::CancelEdit);
                                }
                            },
                            #[name = "save"]
                            gtk::Button {
                                set_label: &fl!(LOCALE, "save"),
                                add_css_class: "suggested-action",
                                connect_clicked[sender, name_edit,public_port_edit, private_port_edit] => move |_| {
                                    let Ok(public_port) = public_port_edit.text().parse() else {
                                        return;
                                    };
                                    let Ok(private_port) = private_port_edit.text().parse() else {
                                        return;
                                    };
                                    sender.input(Message::SaveEdit {
                                        name: name_edit.text().into(),
                                        public_port,
                                        private_port,
                                    });
                                }
                            },
                        }
                    },

                    adw::PreferencesGroup {
                        #[name = "name_edit"]
                        adw::EntryRow {
                            set_title: &fl!(LOCALE, "name"),
                            set_text: &model.name,
                        },
                        #[local_ref]
                        // #[name = "type_select"]
                        comborow -> adw::ComboRow,
                        #[name = "public_port_edit"]
                        adw::EntryRow {
                            set_title: &fl!(LOCALE, "public-port"),
                            set_input_purpose: gtk::InputPurpose::Digits,
                            set_text: &format!("{}",model.port.public_port),
                        },
                        #[name = "private_port_edit"]
                        adw::EntryRow {
                            set_title: &fl!(LOCALE, "private-port"),
                            set_input_purpose: gtk::InputPurpose::Digits,
                            set_text: &format!("{}",model.port.private_port),
                        },
                    }
                }
            }
        }
    }

    // Initialize the UI.
    fn init(
        state: MappingInit,
        root: Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let model = Self {
            name: state.name,
            port: state.ports,
            state: State::Idle,
            editing: false,
            combo_row: SimpleComboRow::builder()
                .launch(SimpleComboRow {
                    variants: vec![Type::Both, Type::Tcp, Type::Udp],
                    active_index: Some(match state.ports.ty {
                        Type::Both => 0,
                        Type::Tcp => 1,
                        Type::Udp => 2,
                    }),
                })
                .detach(),
            spinner: adw::Spinner::builder().build(),
            spinner_present: Cell::new(false),
            icon: gtk::Image::from_icon_name("edit-copy-symbolic"),
            icon_present: Cell::new(false),
            window: state.window,
        };

        let comborow = model.combo_row.widget();
        comborow.set_title("Type");
        let widgets = view_output!();
        ComponentParts { model, widgets }
    }

    fn update(&mut self, msg: Self::Input, sender: ComponentSender<Self>) {
        match msg {
            Message::Edit => self.editing = true,
            Message::SaveEdit {
                name,
                public_port,
                private_port,
            } => {
                self.port.public_port = public_port;
                self.port.private_port = private_port;
                self.port.ty = *self.combo_row.model().get_active_elem().unwrap();
                self.editing = false;
                if self.name != name {
                    self.name = name;
                    sender
                        .output(OutMessage::ChangeName(self.name.clone()))
                        .unwrap();
                }

                sender.output(OutMessage::Save).unwrap();
            }
            Message::CancelEdit => self.editing = false,
            Message::Copy => {
                sender
                    .output(OutMessage::Copy(self.state.port_or(self.port.public_port)))
                    .unwrap();
            }
            Message::StartStop => {
                if self.state.is_running() {
                    sender.output(OutMessage::Stop).unwrap();
                } else {
                    sender.output(OutMessage::Start).unwrap();
                }
                match self.state {
                    State::Idle => self.state = State::Starting,
                    State::Running(_) | State::Stopping => self.state = State::Stopping,
                    State::Error => self.state = State::Stopping,
                    State::Starting => {}
                }
            }
            Message::Delete => {
                let dialog = adw::AlertDialog::builder()
                    .body(fl!(
                        LOCALE,
                        "do-you-want-to-delete",
                        name = self.name.as_str()
                    ))
                    .title("Deletion")
                    .build();
                dialog.add_responses(&[
                    ("Cancel", &fl!(LOCALE, "cancel")),
                    ("Delete", &fl!(LOCALE, "delete")),
                ]);
                dialog.set_close_response("Cancel");
                dialog.set_response_appearance("Delete", adw::ResponseAppearance::Destructive);
                dialog.connect_response(None, move |_, choice| {
                    if choice == "Delete" {
                        sender.output(OutMessage::Delete).ok();
                    }
                });
                dialog.present(Some(&self.window));
            }
            Message::BackendStopped => {
                self.state = State::Idle;
                sender.output(OutMessage::UpdateState(self.state)).ok();
            }
            Message::Running(port) => {
                self.state = State::Running(port);
                sender.output(OutMessage::UpdateState(self.state)).ok();
            }
            Message::Error(err) => {
                self.state = State::Error;
                sender.output(OutMessage::UpdateState(self.state)).ok();
                sender
                    .output_sender()
                    .send(OutMessage::Toast(ToastMessage::new(err)))
                    .ok();
            }
        }
    }
}
